﻿using System.Collections.Generic;

namespace MultiModalBloodSemiQuantResultDataNS 
{
    public class Meta {
        public int limit { get; set; }
        public object next { get; set; }
        public int offset { get; set; }
        public object previous { get; set; }
        public int total_count { get; set; }
    }

    public class Object {
        public string alias { get; set; }
        public string call { get; set; }
        public string @case { get; set; }
        public string code { get; set; }
        public string comments { get; set; }
        public string description { get; set; }
        public int id { get; set; }
        public string identifier { get; set; }
        public string instrument_id { get; set; }
        public string measurement { get; set; }
        public string method { get; set; }
        public string name { get; set; }
        public object @operator { get; set; }
        public object patient_age_range_hi { get; set; }
        public object patient_age_range_lo { get; set; }
        public string patient_gender { get; set; }
        public string range_description { get; set; }
        public string range_hi { get; set; }
        public string range_lo { get; set; }
        public string resource_uri { get; set; }
        public string substance { get; set; }
        public string test { get; set; }
        public string units { get; set; }
    }

    public class RootMultiModalBloodSemiQuantResultData {
        public Meta meta { get; set; }
        public List<Object> objects { get; set; }
    }
}
