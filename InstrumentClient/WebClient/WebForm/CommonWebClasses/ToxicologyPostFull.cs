﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToxicologyPostFullNS
{
    public class ToxicologyPostFull
    {
        public string access_key { get; set; }
        public string accession { get; set; }
        public string added { get; set; }
        public string added_by { get; set; }
        public string auxid { get; set; }
        public bool checked_out { get; set; }
        public object checked_out_by { get; set; }
        public object checked_out_on { get; set; }
        public string customer { get; set; }
        public List<object> diagnosis_codes { get; set; }
        public string diagnosis_codes_order { get; set; }
        public string encounterid { get; set; }
        public List<object> facilities { get; set; }
        public string facilities_order { get; set; }
        public int id { get; set; }
        public List<object> internal_notes { get; set; }
        public string modified { get; set; }
        public string modified_by { get; set; }
        public string notes { get; set; }
        public string order_number { get; set; }
        public List<string> panels { get; set; }
        public string patient { get; set; }
        public List<object> physicians { get; set; }
        public string physicians_order { get; set; }
        public object primary_insurance { get; set; }
        public bool qced { get; set; }
        public object qced_by { get; set; }
        public object qced_on { get; set; }
        public bool released { get; set; }
        public object released_by { get; set; }
        public object released_on { get; set; }
        public object report_uri { get; set; }
        public string resource_uri { get; set; }
        public object secondary_insurance { get; set; }
        public bool signed { get; set; }
        public object signed_by { get; set; }
        public object signed_on { get; set; }
        public string status { get; set; }
        public string type { get; set; }
    }

}
