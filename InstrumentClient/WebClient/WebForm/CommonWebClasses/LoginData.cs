﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginDataNS
{
    public class LoginData
    {
        private string _myLoginid = "";
        private string _myPassword = "";

        public string loginid {
            get { return _myLoginid; }
            set { _myLoginid = value; }
        }

        public string password{
            get { return _myPassword; }
            set { _myPassword = value; }
        }
    }
}
