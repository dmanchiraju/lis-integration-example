﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFormTest.CommonWebClasses
{
    public class URIs
    {
        private string _myAccountUri = "";
        private string _mySelectedUri = "";  //selected by user ComboBox.
        private string _myLoginUri = "";
        private string _myBaseUri = "";
        private string _myReferUri = "";

        //as a reality check, try to get a return code from the account  URL.
        public string accountUri {
            get { return _myAccountUri; }
            set { _myAccountUri = value; }
        }

        public string selectedUri {
            get { return _mySelectedUri; }
            set { _mySelectedUri = value; }
        }
        public string loginUri {
            get { return _myLoginUri; }
            set { _myLoginUri = value; }
        }
        public string baseUri {
            get { return _myBaseUri; }
            set { _myBaseUri = value; }
        }
        public string referUri{
            get { return _myReferUri; }
            set { _myReferUri = value; }
        }
    }
}
