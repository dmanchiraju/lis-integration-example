﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToxicologyPostMinimumNS
{
    public class ToxicologyPostMinimum
    {
        public string customer { get; set; }
        public List<string> panels { get; set; }
        public string patient { get; set; }
        public string status { get; set; }
        public string type { get; set; }
    }

    public class ToxicologyPostMinimumChad
    {
        public List<object> diagnosis_codes { get; set; }
        public List<object> facilities { get; set; }
        public List<object> internal_notes { get; set; }
        public List<string> panels { get; set; }
        public string patient { get; set; }
        public List<object> physicians { get; set; }
        public string status { get; set; }
        public string type { get; set; }
    }

}
