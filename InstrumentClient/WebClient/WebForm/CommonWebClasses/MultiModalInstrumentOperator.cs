﻿using System.Collections.Generic;

namespace MultiModalInstrumentOperatorNS 
{
    public class Meta {
        public string previous { get; set; }
        public string total_count { get; set; }
        public string offset { get; set; }
        public string limit { get; set; }
        public string next { get; set; }
    }

    public class Object 
    {
        public string dept { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string middle_name { get; set; }
        public string suffix { get; set; }
        public string title { get; set; }
        public string resource_uri { get; set; }
        public string prefix { get; set; }
        public string id { get; set; }

        public Object()
        {
            
        }
    }

    public class RootMultiModalInstrumentOperator {
        public Meta meta { get; set; }
        public List<Object> objects { get; set; }
    }
}
