﻿using System.Collections.Generic;

namespace SpecimenDataNS
{
    public class Meta {
        public int limit { get; set; }
        public object next { get; set; }
        public int offset { get; set; }
        public object previous { get; set; }
        public int total_count { get; set; }
    }

    public class Object {
        public string body_location { get; set; }
        public string @case { get; set; }
        public string collection_date { get; set; }
        public string description { get; set; }
        public int id { get; set; }
        public string label { get; set; }
        public string method { get; set; }
        public string received_date { get; set; }
        public string resource_uri { get; set; }
        public string shipping_carrier { get; set; }
        public string source { get; set; }
        public string stype { get; set; }
        public string tracking_number { get; set; }
    }

    public class RootSpecimenData {
        public Meta meta { get; set; }
        public List<Object> objects { get; set; }
    }

}//namespace