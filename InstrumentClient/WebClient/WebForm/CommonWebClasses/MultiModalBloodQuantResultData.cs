﻿using System.Collections.Generic;

namespace MultiModalBloodQuantResultDataNS
{
    public class Meta
    {
        public int limit { get; set; }
        public object next { get; set; }
        public int offset { get; set; }
        public object previous { get; set; }
        public int total_count { get; set; }
    }

    public class Object
    {
        public string alert_range_description { get; set; }
        public string alert_range_hi { get; set; }
        public string alert_range_lo { get; set; }
        public string alias { get; set; }
        public string @case { get; set; }
        public string code { get; set; }
        public string comments { get; set; }
        public string description { get; set; }
        public string flag { get; set; }
        public string id { get; set; }
        public string identifier { get; set; }
        public string instrument_id { get; set; }
        public string measurement { get; set; }
        public string method { get; set; }
        public string name { get; set; }
        public string normal_range_description { get; set; }
        public string normal_range_hi { get; set; }
        public string normal_range_lo { get; set; }
        public string @operator { get; set; }
        public string patient_age_range_hi { get; set; }
        public string patient_age_range_lo { get; set; }
        public string patient_gender { get; set; }
        public string resource_uri { get; set; }
        public string substance { get; set; }
        public string test { get; set; }
        public string units { get; set; }
    }

    public class RootMultiModalBloodQuantResultData
    {
        public Meta meta { get; set; }
        public List<Object> objects { get; set; }
    }

}//ns