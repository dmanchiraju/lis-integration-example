﻿using System;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json;


namespace WebFormTest 
{
    public partial class Form1: Form 
    {

        //Combine textbox append and data logging functions
        //----------------------------------------------------------------------------------------------------------
        public void CombinedLog(object objectToLog) {
            tbDebug.AppendText(objectToLog.ToString());
            LogObject(objectToLog);
        }
        //----------------------------------------------------------------------------------------------------------
        private string LogObject(object objToLog) {
            //timestamp info is embedded in JSON; no need to stamp here.
            //var local = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            var univ = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");

            string path = System.AppDomain.CurrentDomain.BaseDirectory;
            string objString = JsonConvert.SerializeObject(objToLog, Formatting.None);

            //before saving, get rid of backslashes, so it looks like real JSON
            string outString = objString.Replace(@"\", "");
            char[] trimChars = { '"' };
            outString = outString.Trim(trimChars);

            using (StreamWriter outputFile = new StreamWriter(path + @"\ClientLog.json", true)) {
                outputFile.WriteLine("\r\n" + univ + objString);
                //outputFile.WriteLine("\r\n " + outString);
            }
            return outString;
        }


    }
}
