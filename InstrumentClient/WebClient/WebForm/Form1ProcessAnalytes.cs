﻿using System;
using RestSharp;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System.Windows.Forms;

using CaseDataNS;
using MultiModalBloodQuantResultDataNS;
using MultiModalBloodSemiQuantResultDataNS;
using SpecimenDataNS;

namespace WebFormTest
{
    public partial class Form1: Form
    {
        //These are used to process Analyte data
        private List<String> specimenIDList = new List<string>();
        private List<RootSpecimenData> rootSpecimenDataList = new List<RootSpecimenData>();
        private RootMultiModalBloodQuantResultData rootMmBQList = new RootMultiModalBloodQuantResultData();
        private RootMultiModalBloodSemiQuantResultData rootMmBSemiQList = new RootMultiModalBloodSemiQuantResultData();               
        private string caseUri; 


        /***********************************************************************************************************
         * 0.RetrieveAnalytesBySpecimenIDs()
         * - Called once the CLS has scanned all SpecimenIDs, and clicked the "Scan Complete" button.
         * - This is the Root Method for retrieving all web API information associated with the Instrument tests
         *   to be performed
         * - tag TODO: I'm assuming that ALL SpecimenIDs in the List point to the same set of Analytes
         * - tag TODO: but, I'm retrieving as lists if this isn't the case.  No error checking for this yet.
         * - Technically, it should only be necessary to use one SpecimenID to retrieve all info;
         * - However, operating on entire list could detect unmapped or swapped specimens.
         *
         ***********************************************************************************************************/
        private void RetrieveAnalytesBySpecimenIDs(List<String>  spIdList)
        {
            try{
                foreach (string spId in spIdList)
                {
                    RootSpecimenData rsd = new RootSpecimenData();
                    rsd = GetCaseBySpecimenID(spId); //a potential List of Cases associated with Specimen

                    if (rsd.meta.total_count == 0){  //no specimen ID found matching scanned value!
                        CombinedLog("\r\nSpecimenID value not found in DB!\r\n");
                        continue;
                    }
                    rootSpecimenDataList.Add(rsd); //otherwise, add to list.
                }
            }
            catch (Exception exception){
                CombinedLog("\r\nERROR: Specimen ID retrieval from DB failed!\r\n");
                CombinedLog(exception.ToString());
            }

            if (rootSpecimenDataList.Count == 0) {
                CombinedLog("\r\nSTOPPING specimen processing; invalid data\r\n");
                return;
            }

            if (rootSpecimenDataList[0].meta.total_count == 0){
                CombinedLog("\r\nInvalid: No items found in SpecimenID List\r\n");
                return;
            }

            //TODO: Only handling a single case mapped to the first specimen.  Add checking for multiple specimens
            caseUri = rootSpecimenDataList[0].objects[0].@case;

            //TODO: Add error checking for accessionID
            string accessionID = GetAccessionNumberByCase(caseUri); //retrieve Case to figure out accession number.

            //TODO: add return type, error checking
            GetMmBloodQuantResultListByAccNumber(accessionID); //populate a List of MultiModalBloodQuantResultData results            
            GetMmBloodSemiQuantResultListByAccNumber(accessionID); //populate a List of MultiModalBloodSemiQuantResultData
        }

        /***********************************************************************************************************
        * 1.Brandon's #6: GET /api/v1/specimen/?label=S12345
        *  GetCaseBySpecimenID(spIdList)
        *  - Use GET by spId to retrieve a List of Case Objects;
        *  - DeSerialize into RootSpecimen Class
        *  - Validate that ALL specimenIDs point to same CASE.
        *  - ex: https:*genalyte-beta.pathagility.com/api/v1/specimen/?label=S1
        *  - Determine the Case URI, and call GetCase(caseURI)
        ***********************************************************************************************************/
        private RootSpecimenData GetCaseBySpecimenID(string spId)
        {
            spId = spId.TrimEnd('\r', '\n'); //remove trailing CR LF
            RootSpecimenData rsd = new RootSpecimenData();

            /*TODO: Specimens in genalyte-beta DB have non-numeric labels atm.  If labels are numeric, enable this code.
            try {
                spIdInt = Convert.ToInt32(spId);
            }
            catch(Exception exception) {
                CombinedLog("\r\nInvalid: SpecimenID > 0 and not null\r\n");
                CombinedLog(exception.ToString());
                return sd;
            }
            if (spIdInt < 1){
                CombinedLog("\r\nInvalid: Specimen Id ID must be > 0 and not null\r\n");
                return sd;
            }
            */
            //GET SpecimenID containing Case
            CombinedLog("\r\n\r\nRetrieving SpecimenID # "+spId +"\r\n");

            var getUriSpecimenID = "api/v1/specimen/?label=" + spId;

            clientA.BaseUrl = new Uri(uris.referUri + getUriSpecimenID);
            clientA.AddDefaultHeader("Referer", uris.referUri);

            var request = new RestRequest("/", Method.GET);
            request.Credentials = new NetworkCredential(login.loginid, login.password);

            IRestResponse response = null;
            try {
                response = clientA.Execute(request);
                CookiesToCookieList(masterCookieList, response);
            } catch (Exception exception) {
                CombinedLog("GetCaseBySpecimenID: response Exception \r\n");
                CombinedLog(exception + "\r\n");
                return null;
            }

            if (response.StatusDescription == "Not Found") {
                CombinedLog("\r\nInvalid Specimen Number\r\n");
                return rsd;
            }

            CombinedLog("Specimen #"+spId +" logged to file\r\n");
            CombinedLog("Results: \r\n" + response.Content);

            try {
                rsd = JsonConvert.DeserializeObject<RootSpecimenData>(response.Content);
                CombinedLog(rsd.ToString());
            }
            catch (Exception exception) {
                CombinedLog("\r\n\r\nRootSpecimenData deserialization failed!\r\n");
                CombinedLog("\r\n\r\n" + exception.ToString() +  "\r\n");
            }

            var numCases = Convert.ToUInt32(rsd.meta.total_count);
            if (numCases == 0) {
                CombinedLog("\r\n\r\n...No Case associated with Specimen Found!\r\n");
            }
            if (numCases > 1){
                CombinedLog("\r\n\r\n...More than one Case associated with retrieved Specimen!\r\n");
            }
            for(int i=0; i<numCases; i++) 
            {
                if (rsd.objects[i].@case == null) {
                    CombinedLog("...Case URI in Specimen " + i +" is null. Invalid Specimen ID?\r\n");
                }
            }

            return rsd;
        }//GetCaseBySpecimenID

        /***********************************************************************************************************
        *  2.Brandon's #7: GET /api/v1/case/18/
        *    GetAccessionNumberByCase(caseUri):
        *    - A Case will provide the accession string.
        ***********************************************************************************************************/
        private string GetAccessionNumberByCase(string caseUri )
        {
            CaseData cd = new CaseData();

            clientA.BaseUrl = new Uri(uris.referUri + caseUri);
            clientA.AddDefaultHeader("Referer", uris.referUri);

            var request = new RestRequest("/", Method.GET);
            request.Credentials = new NetworkCredential(login.loginid, login.password);

            IRestResponse response = null;
            try {
                response = clientA.Execute(request);
                CookiesToCookieList(masterCookieList, response);
            }
            catch (Exception exception){
                CombinedLog("Case: response Exception \r\n");
                CombinedLog( exception +"\r\n");
                return "Case: response Exception ";
            }

            if (response.StatusDescription == "Not Found") {
                CombinedLog("\r\nInvalid Case Number\r\n");
                return "Not Found";
            }
            CombinedLog("Case: " + caseUri + " logged to file\r\n");
            CombinedLog("Results: \r\n" + (response.Content));

            try {
                cd = JsonConvert.DeserializeObject<CaseData>(response.Content);
                LogObject(cd);
            }
            catch (Exception exception) {
                CombinedLog("\r\n\r\nCase Object deserialization failed!\r\n");
                CombinedLog("\r\n\r\n" + exception.ToString() + "\r\n");
            }

            string myAccession = cd.accession;

            if (myAccession == null) {
                CombinedLog("\r\n\r\n...Null accession string!\r\n");
                return "accession number null!";
            }
            else {
                CombinedLog("\r\n...accession string = " + myAccession + "\r\n");
            }

            return myAccession;
        }//GetAccessionNumberByCase

        /***********************************************************************************************************
         *3a. Brandon's #8a: GET /api/v1/mmbloodquantresult/?case__accession=BLD-00003  
         *    GetMmBloodQuantResultListByAccNumber(string accNumber):  all tests linked to accession number
         *    
         *  - This will result in a List of Analyte URIs; each URI in the List must be invoked in order to PATCH in an AnalyteResult.
         *  
         *  - These are not implemented:
         *    - GET /api/v1/mmbloodquantresult/?case__accession=BLD-00003&identifier=Hematocrit
         *    - GetMMBloodQuantResultByAccNumber(string accNumber, string identifier): get specific test linked to accession number
         ***********************************************************************************************************/
        private void  GetMmBloodQuantResultListByAccNumber(string accessionID)
        {
            var getMMBloodQuantList = "api/v1/mmbloodquantresult/?case_accession=" + accessionID ;

            RootMultiModalBloodQuantResultData mm = new RootMultiModalBloodQuantResultData();

            clientA.BaseUrl = new Uri(uris.referUri + getMMBloodQuantList);
            clientA.AddDefaultHeader("Referer", uris.referUri);

            var request = new RestRequest("/", Method.GET);

            request.Credentials = new NetworkCredential(login.loginid, login.password); 

            IRestResponse response = null;
            try {
                response = clientA.Execute(request);
                CookiesToCookieList(masterCookieList, response);
            } 
            catch (Exception exception) {
                CombinedLog("GetMmBloodQuantResultListByAccNumber: response Exception \r\n");
                CombinedLog(exception + "\r\n");
                return;
            }

            if (response.StatusDescription == "Not Found") {
                CombinedLog("\r\nNo MmBloodQuantResults found via Accession ID\r\n");
                return;
            }
            CombinedLog("MmBloodQuantresultList: " + getMMBloodQuantList + " logged to file\r\n");
            CombinedLog("Results: \r\n" + response.Content);

            try {
                mm = JsonConvert.DeserializeObject<RootMultiModalBloodQuantResultData>(response.Content);
                LogObject(mm);
            } catch (Exception exception) {
                CombinedLog("\r\n\r\nMultiModalBloodQuantList Object deserialization failed!\r\n");
                CombinedLog("\r\n\r\n" + exception.ToString() + "\r\n");
                LogObject(exception.ToString());
            }

            rootMmBQList = mm;  //we have captured all Tests (Analytes) associated with the Accession Number.

        }//GetMmBloodQuantResultListByAccNumber

        /***********************************************************************************************************
         *3b. Brandon's #8b:  GET /api/v1/mmbloodsemiquantresult/?case__accession=BLD-00001 GETs a List of test objects
         *    GetMmBloodSemiQuantResultListByAccNumber(string accNumber):  all tests linked to accession number
         *
         *  - This will result in a List of Analyte URIs; each URI in the List must be invoked in order to PATCH in an AnalyteResult.
         ***********************************************************************************************************/
        private void GetMmBloodSemiQuantResultListByAccNumber(string accessionID) 
        {
            var getMMBloodSemiQuantList = "api/v1/mmbloodsemiquantresult/?case_accession=" + accessionID;

            RootMultiModalBloodSemiQuantResultData mm = new RootMultiModalBloodSemiQuantResultData();

            clientA.BaseUrl = new Uri(uris.referUri + getMMBloodSemiQuantList);
            clientA.AddDefaultHeader("Referer", uris.referUri);

            var request = new RestRequest("/", Method.GET);

            request.Credentials = new NetworkCredential(login.loginid, login.password); 

            IRestResponse response = null;
            try {
                response = clientA.Execute(request);
                CookiesToCookieList(masterCookieList, response);
            } 
            catch (Exception exception) {
                CombinedLog("GetMmBloodSemiQuantResultListByAccNumber: response Exception \r\n");
                CombinedLog(exception + "\r\n");
                return;
            }

            if (response.StatusDescription == "Not Found") {
                CombinedLog("\r\nNo MmBloodSemiQuantResults found via Accession ID\r\n");
                return;
            }
            string mmBloodSemiQuantContent = LogObject(response.Content);
            CombinedLog("MmBloodSemiQuantresultList: " + getMMBloodSemiQuantList + " logged to file\r\n");
            CombinedLog("Results: \r\n" + mmBloodSemiQuantContent);

            try {
                mm = JsonConvert.DeserializeObject<RootMultiModalBloodSemiQuantResultData>(response.Content);
                LogObject(mm);
            } catch (Exception exception) {
                CombinedLog("\r\n\r\nMultiModalBloodSemiQuantList Object deserialization failed!\r\n");
                CombinedLog("\r\n\r\n" + exception.ToString() + "\r\n");
                LogObject(exception.ToString());
            }

            rootMmBSemiQList = mm;  //we have captured all Tests (Analytes) associated with the Accession Number.

        }//GetMMBloodSemiQuantResultListByAccNumber

        /***********************************************************************************************************
         * *4.Brandon's #9
         *  - PatchAnalyteResultByAnalyteResourceUri(string panelResourceURI, string identifier, string identifierName)
         *  - Most of the fields in the "analyte object" were pre-populated when the tests were accessioned.
         *  - Instrument test results are PATCHed into the Analyte object.
         ***********************************************************************************************************/
        void ExecuteAnalytePatch(string patchResourceURI, string identifier, string identifierName)
        {
            patchResourceURI = patchResourceURI.TrimStart('/');

            RestClient client = new RestClient();
            client.CookieContainer = masterCookieContainer;
            client.AddDefaultHeader("Accept", "application/json");
            client.AddDefaultHeader("Referer", uris.referUri);
            client.AddDefaultHeader("X-CSRFToken", masterCookieList[2].Value);
            client.AddDefaultHeader("Cache-Control", "no-cache");
            client.BaseUrl = new Uri(uris.referUri + patchResourceURI);

            var request = new RestRequest("/", Method.PATCH);
            request.Credentials = new NetworkCredential(login.loginid, login.password);

            request.AddHeader("Accept", "application/json");
            request.RequestFormat = DataFormat.Json;
            //tag TODO: a more concise way of populating JSON body
            object jsonBody;
            switch (identifier) {
                case "measurement": {
                    jsonBody = new { measurement = identifierName };
                    request.AddJsonBody(jsonBody);
                    break;
                }
                case "instrument_id": {
                    jsonBody = new { instrument_id = identifierName };
                    request.AddJsonBody(jsonBody);
                    break;
                }
                case "operator": {
                    jsonBody = new { @operator = identifierName };
                    request.AddJsonBody(jsonBody);
                    break;
                }
                default: {
                    CombinedLog("\r\n\r\nInvalid Identifier passed to PatchAnalyteResultByAnalyteResourceUri()");
                    break;
                }
            }

            IRestResponse response = null;
            try {
                response = client.Execute(request);
                CookiesToCookieList(masterCookieList, response);
            } 
            catch (Exception exception) {
                CombinedLog("ExecuteAnalytePatch: response Exception \r\n");
                CombinedLog(exception + "\r\n");
                return;
            }

            if (response.StatusDescription == "Not Found") {
                CombinedLog("\r\nMmBloodSemiQuantResults not found via Accession ID\r\n");                
            }
        }//ExecuteAnalytePatch

        /***********************************************************************************************************
         *5. PostToTriggerCaseUpdate(string caseUri)
         *  NOTE that this is called by Form1Handlers; see Form1Handlers.cs, HandleInstrumentnameData() methods.
         *  
         *  After all data for an instrument has been PATCHed, call this method to trigger server to analyze results.
         *  Brandon: "Once all search-and-PATCHs to set instrument results have been completed, the the instrument interface
         *  client must force the case, now with its new measurements, to be reinterpreted, like this:
         *  
         *  POST /api/v1/multimodalcase/
         *  Using this structure
         *  {
         *    "case":"/api/v1/multimodalcase/16/",
         *    "method":"interpret",
         *    "kwargs":{"overwrite_data":true}
         *  }
         *  "
         ***********************************************************************************************************/
        private void PostTriggerCaseUpdate(string caseUri)
        {
            CombinedLog("\r\nPostToTriggerCaseUpdate executing\r\n");

            string myCaseUri = caseUri.Replace("case", "multimodalcase");            
            myCaseUri = myCaseUri.TrimStart('/');

            string mmCaseUri = "api/v1/multimodalcasemethod/"; //used for dest address only; constant

            RestClient client = new RestClient();
            client.CookieContainer = masterCookieContainer;
            client.AddDefaultHeader("Accept", "application/json");
            client.AddDefaultHeader("Referer", uris.referUri);
            client.AddDefaultHeader("X-CSRFToken", masterCookieList[2].Value); 
            client.AddDefaultHeader("Cache-Control", "no-cache");
            client.BaseUrl = new Uri(uris.referUri + mmCaseUri);

            var request = new RestRequest("/", Method.POST);
            request.Credentials = new NetworkCredential(login.loginid, login.password); 

            request.AddHeader("Accept", "application/json");
            request.RequestFormat = DataFormat.Json;

            object jsonBody = new {
                @case = myCaseUri,
                method = "interpret",
                kwargs = new {overwrite_data = "true"}
            };

            request.AddJsonBody(jsonBody);

            IRestResponse response = null;
            try {
                response = client.Execute(request);
                CookiesToCookieList(masterCookieList, response);
            } catch (Exception exception) {
                CombinedLog("PostTriggerCaseUpdate: response Exception \r\n");
                CombinedLog(exception + "\r\n");
                return;
            }


            //response.StatusDescription == "Created" for success
            if (response.StatusDescription == "Not Found") {
                CombinedLog("\r\nPostTriggerCaseUpdate failed\r\n");
                return;
            }
        }//PostTriggerCaseUpdate
    }//class Form1
}
