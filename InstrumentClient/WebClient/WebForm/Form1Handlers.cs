﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using GenalyteMobile.DeviceData;

namespace WebFormTest
{
    public partial class Form1: Form
    {
      /***********************************************************************************************************
        * NOTE FOR PATIENT DATA:
        * 
        * IT is currently assumed that all Analyte Objects have been retrieved from the DB by the time the 
        * corresponding Handler is called by the Instrument code.  This assumes that:
        * - The CLS barcode-scanned the tubes with this application before starting an Instrument test.
        * - The time required to retrieve all "partially populated" Analyte information from the DB is always 
        *   less than the amount of time it takes to perform an Instrument test.
        * - There are no problems reaching the LIS...
        ***********************************************************************************************************/   
        //----------------------------------------------------------------------------------------------------------
        //Piccolo methods.                 
        //----------------------------------------------------------------------------------------------------------
        private void btnPiccoloControlData_Click(object sender, EventArgs e) {
            CombinedLog("\r\nPiccolo Control Data currently undefined\r\n");
        }

        //----------------------------------------------------------------------------------------------------------
        private void btnPiccoloPatientData_Click(object sender, EventArgs e) {
            CombinedLog("\r\nCreating Piccolo patient object\r\n");
            PiccoloPatientData pPatientData = new PiccoloPatientData(); //ctor fills in meaningless text for now.           

            CombinedLog("Calling Piccolo HandlePiccoloPatientData()\r\n");
            HandlePiccoloPatientData(pPatientData);
        }

        //----------------------------------------------------------------------------------------------------------
        /* Supported Piccolo Analytes to be updated: ALB, TP, ALP, ALT, AST
         * Additional fields to be updated: instrument_id, operator URI TODO: Operator being eliminated
         */
        public void HandlePiccoloPatientData(PiccoloPatientData ppData)
        {
            CombinedLog("\r\n HandlePiccoloPatientData:\r\n");            
            var analyteObjectList = rootMmBQList.objects; //tag TODO redundant: Debug only

            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, ppData, "ALB");//tag confirmed DB backend update 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, ppData, "TP");//tag confirmed DB backend update 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, ppData, "ALP");//tag confirmed DB backend update 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, ppData, "ALT");//tag confirmed DB backend update 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, ppData, "AST");//tag confirmed DB backend update 5/31

            PostTriggerCaseUpdate(caseUri);
        }

        //----------------------------------------------------------------------------------------------------------
        //Piccolo only.
        public void PatchAnalyteResultByAnalyteResourceUri(
                            List <MultiModalBloodQuantResultDataNS.Object> analyteObjectList,
                            PiccoloPatientData ppData, string analyteName)
        {
            MultiModalBloodQuantResultDataNS.Object analyteObject = new MultiModalBloodQuantResultDataNS.Object();
            try {
                //TODO: explore options here: https://stackoverflow.com/questions/1175645/find-an-item-in-list-by-linq
                analyteObject = analyteObjectList.Single(s => s.identifier == analyteName);

                //Need to PATCH: measurement, Instrument serial number, operatorID using patchUri.
                string patchUri = analyteObject.resource_uri;
                string measurement = analyteObject.measurement;   //tag TODO: I'm NOT changing the measurement for now.    
                string instrumentID = ppData.serialNumber;
                //string operatorID = login.loginid;
                string operatorID = "no DB endpoint as of 5/26";
                string debugOutput = "analyte: " + analyteName + ", patchUri: " + patchUri + ", measurement: " + measurement + ", instrumentID: " +
                                     instrumentID + ", operatorID: " + operatorID + "\r\n";

                CombinedLog(debugOutput);

                ExecuteAnalytePatch(patchUri, "measurement", measurement);
                ExecuteAnalytePatch(patchUri, "instrument_id", instrumentID);
                //ExecuteAnalytePatch(patchUri, "operator", operatorID); TODO being eliminated

            } catch (Exception e) {
                CombinedLog("HandlePiccoloPatientData: Piccolo DB object not found");
                CombinedLog(e.ToString());
            }
        }

        //----------------------------------------------------------------------------------------------------------
        //Emerald Methods
        //----------------------------------------------------------------------------------------------------------
        private void btnEmeraldControlData_Click(object sender, EventArgs e) {
            CombinedLog("\r\nEmerald Control Data currently undefined\r\n");
        }
        //----------------------------------------------------------------------------------------------------------
        private void btnEmeraldPatientData_Click(object sender, EventArgs e) {
            CombinedLog("\r\nCreating Emerald patient object\r\n");
            EmeraldPatientData ePatientData = new EmeraldPatientData();
            
            CombinedLog("Calling Emerald HandleEmeraldPatientData()\r\n");
            HandleEmeraldPatientData(ePatientData);
        }

        //----------------------------------------------------------------------------------------------------------
        public void HandleEmeraldPatientData(EmeraldPatientData epData)
        {
            CombinedLog("HandleEmeraldPatientData\r\n");
            
            var analyteObjectList = rootMmBQList.objects; //tag TODO redundant: Debug only
            //pass in AnalyteDBIdentifier, AnalyteInstrumentIdentifier
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "WBC",   "WBC");          //DB: "WBC"     //tag DB backend OK 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "RBC",   "RBC");          //DB: "RBC"     //tag DB backend OK 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "HGB",   "HGB");          //DB: "Hgb"     //tag DB backend OK 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "HCT",   "HCT");          //DB: "Hct"     //tag DB backend OK 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "LYM%",  "LYMPercent");   //DB: "LYM%"    //tag DB backend OK 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "LYM#", "LYM");          //DB: "LYM#"     //tag DB backend OK 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "Gran%", "GRAPercent");   //DB: "Gran%"   //tag DB backend OK 5/31
            PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "Gran#", "GRA");          //DB: "Gran#"   //tag DB backend OK 5/31

            //TODO: Emerald provides these values; no corresponding endpoints in DB. email sent to Pathagility 5/23/2017            
            //PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "MCV");         
            //PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "MCH");         
            //PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "MCHC");        
            //PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "RDW");         
            //PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "PLT");         
            //PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "MPV");         
            //PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "MIDPercent");    
            //PatchAnalyteResultByAnalyteResourceUri(analyteObjectList, epData, "MID");           

            PostTriggerCaseUpdate(caseUri);
        }

        //----------------------------------------------------------------------------------------------------------
        //Emerald only
        public void PatchAnalyteResultByAnalyteResourceUri(
                        List<MultiModalBloodQuantResultDataNS.Object> analyteObjectList,
                        EmeraldPatientData epData, string analyteDBName, string analyteInstName) 
        {

            MultiModalBloodQuantResultDataNS.Object analyteObject = new MultiModalBloodQuantResultDataNS.Object();
            try {
                //TODO: explore options here: https://stackoverflow.com/questions/1175645/find-an-item-in-list-by-linq
                //analyteObject = analyteObjectList.Single(s => s.identifier == analyteDBName); //case sensitive.
         
                //https://stackoverflow.com/questions/5312585/linq-case-insensitive-without-toupper-or-tolower
                analyteObject = analyteObjectList.Single(s => s.identifier.Equals(analyteDBName, StringComparison.OrdinalIgnoreCase));

                //Need to PATCH: measurement, Instrument serial number, operatorID using patchUri.
                string patchUri     = analyteObject.resource_uri;
                string measurement  = analyteObject.measurement;  //tag TODO: I'm not changing the measurement for now.
                string instrumentID = epData.serialNumber;
                //string operatorID = login.loginid;     //tag TODO being eliminated in DB
                //string diskLotNum = epData.diskLotNumber;       //tag TODO: there is no DB entry for this
                
                string operatorID = "no DB endpoint as of 5/26";
                string debugOutput = "analyte(DB/Instrument): " +analyteDBName+ "/"+analyteInstName+ 
                                     ", patchUri: " +patchUri+ ", measurement: " +measurement+ 
                                     ", instrumentID: " +instrumentID+ ", operatorID: " +operatorID+ "\r\n";

                CombinedLog(debugOutput);

                ExecuteAnalytePatch(patchUri, "measurement", measurement);
                ExecuteAnalytePatch(patchUri, "instrument_id", instrumentID);
                //ExecuteAnalytePatch(patchUri, "operator", operatorID);

            } catch (Exception e) {
                CombinedLog("HandleEmaeraldPatientData: Emerald DB object not found");
                CombinedLog(e.ToString());
            }
        }
        //----------------------------------------------------------------------------------------------------------
        //iSED methods
        //----------------------------------------------------------------------------------------------------------
        private void btnIsedControlData_Click(object sender, EventArgs e) {
            CombinedLog("\r\niSED Control Data currently undefined\r\n");
        }
        //----------------------------------------------------------------------------------------------------------
        private void btnIsedPatientData_Click(object sender, EventArgs e) {
            CombinedLog("\r\nCreating iSED patient object\r\n");
            iSEDPatientData iPatientData = new iSEDPatientData();
            CombinedLog("Calling iSED HandleISedPatientData()\r\n");
            HandleiSEDPatientData(iPatientData);
        }
        //----------------------------------------------------------------------------------------------------------
        public void HandleiSEDPatientData(iSEDPatientData ipData)
        {
            CombinedLog("HandleiSEDPatientData\r\n");           
        }

    }//class
}//ns
