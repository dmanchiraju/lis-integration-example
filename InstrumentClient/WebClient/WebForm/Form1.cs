﻿using System;
using System.IO;
using RestSharp;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using LoginDataNS;
using UriDataNS;
using ToxicologyPostFullNS;


/***********************************************************************************************************
 * TODO: confirm validity of comments
 *  Requirements of Scanner Code:
 *  - The Phlebotomist initiates the accessioning process by linking the tube barcodes (SpecimenID(s))
 *    to one or more tests to be performed.  This step is not handled by this Application.
 *
 *  - The CLS receives the tube(s) from the Phlebotomist, and barcode scans them again with this Application.
 *  - Each barcode is a SpecimenID, which acts as a key for retrieving information on all tests to
 *    be performed.
 *  - A group of tests is called a Panel; as of 6/1/2017, there's nothing in the web API that defines a Panel.
 *
 *  - This Application traverses the web API to retrieve the List instrument tests (Analyte Objects) that
 *    will be populated by corresponding Instrument test result data.
 *
 *  - Based on the Physician's Test Request, each Analyte Object corresponding to a particular test has
 *    already been instantiated.  Only the results of the test need to be PATCHed into the Analyte Object
 *    by this Application.
 *
 *  - Maximum possible number of tests per Instrument:
 *  - Piccolo: up to 15 tests
 *  - Emerald: up to 16 tests
 *  - iSED:    only 1 test defined?
 *
 *  General algorithm:
 *  - CLS clicks "Start Scan" button to direct focus to "SpecimenID Scan" textbox.
 *  - CLS scans N barcodes on tubes.
 *  - Check for duplicate entries:
 *    - TODO Do we allow duplicate entries in the same session? <<<<<<<<<<<<<<<<<<<<<<
 *  - Foreach barcode:
 *    - traverse web API to confirm that the corresponding endpoint exists.
 *    - TODO confirm that each SpecimenID belongs to the same patient??
 *    - retrieve and deserialize the analyte (test) objects that correspond to the Physician's order
 *
 *  - When Instrument data is collected, the Handler type for that instrument will be called.  See
 *    "Requirements of all Handlers:"
 *
 ***********************************************************************************************************/

namespace WebFormTest
{
    public partial class Form1: Form
    {
        public LoginData login = new LoginData();
        public UriData   uris   = new UriData();

        private CookieContainer masterCookieContainer = new CookieContainer();
        private List<KeyValuePair<string, string>> masterCookieList = new List<KeyValuePair<string, string>>();
        private RestClient clientA = new RestClient();  //TODO: containing Client info here.  Options?

        private bool loginSuccess = false;
        public bool pathagilitySiteUp = false;
        private bool form1Closed = false; //check this to kill connectivity thread.

        public Thread connectThread;

        public Form1()
        {
            InitializeComponent();

            try {
                //throw new Exception("Form 1 Initialization"); tag ExceptionTest
                CombinedLog("\r\nGUI initialized...");
                CombinedLog("\r\nChecking connectivity...");

                //put Pathagility site status in different thread to support periodic checking
                ThreadStart connectRef = new ThreadStart(CheckConnectivity);
                connectThread = new Thread(connectRef);
                connectThread.Start();

                Thread.Sleep(2000); //wait for CheckConnectivity to do a GET
                if (pathagilitySiteUp)
                {
                    CombinedLog("\r\nDestination is up");
                    tbConnectStatus.BackColor = Color.GreenYellow;
                }
                else
                {
                    CombinedLog("\r\nDestination is down");
                    tbConnectStatus.BackColor = Color.Tomato;
                }
            }
            catch (Exception exception){
                CombinedLog("\r\nAPPLICATION INITIALIZATION FAILURE\r\n");
                CombinedLog(exception.ToString());
            }
        }
        //---------------------------------------------------------------------------------------
        private void comboSelectDB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboSelectDB.SelectedItem.ToString() == "genalyte-beta")
            {
                uris.selectedUri = "genalyte-beta";
                uris.loginUri = @"https://genalyte-beta.pathagility.com/api/login/";
                uris.baseUri = @"https://genalyte-beta.pathagility.com/";
                uris.referUri =  @"https://genalyte-beta.pathagility.com/";
                CombinedLog("\r\nselected DB: " + uris.selectedUri);
            }

            else if (comboSelectDB.SelectedItem.ToString() == "genalyte-demo")
            {
                uris.selectedUri = "genalyte-demo";
                uris.loginUri = @"https://genalyte-demo.pathagility.com/api/login/";
                uris.baseUri = @"https://genalyte-demo.pathagility.com/";
                uris.referUri =  @"https://genalyte-demo.pathagility.com/";

                CombinedLog( "\r\ngenalyte-demo selected: Connection unsupported");
            }
        }
        //---------------------------------------------------------------------------------------
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (loginSuccess == true) {
                CombinedLog("\r\nAlready logged in. Please close application to log in again.\r\n");
                return;
            }
            if (uris.selectedUri != "genalyte-beta") {
                CombinedLog("\r\nDatabase connection unsupported. Please select another Database." + "\r\n");
                return;
            }

            //TODO tag while debugging, use real credentials...
            CombinedLog("\r\n\r\nUser Name: Instrument_User"+ "\r\n");
            CombinedLog("Password: ***********" + "\r\n");
            login.loginid = "Instrument_User";
            login.password = "InstrumentN2017";

            //--------------------------------------------------------------------------------------------
            //1: GET to login Uri, just to get CSRFToken
            //--------------------------------------------------------------------------------------------
            CombinedLog("...starting login step 1" + "\r\n");

            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            clientA.BaseUrl = new Uri(uris.loginUri);
            clientA.CookieContainer = masterCookieContainer;

            clientA.AddDefaultHeader("Accept", "application/json");

            var request = new RestRequest("/", Method.GET);

            try {
                IRestResponse response = clientA.Execute(request);
                CookiesToCookieList(masterCookieList, response);
            }
            catch (Exception exception) {
                CombinedLog("\r\nbtnLogin_Click(): Step 1: Server response failure\r\n");
                CombinedLog("\r\n" + exception);
                return;
            }
            //--------------------------------------------------------------------------------------------
            //2: Next, POST to same login Uri, to get yet another CSRFToken
            //--------------------------------------------------------------------------------------------
            CombinedLog("...starting login step 2" + "\r\n");

            clientA.AddDefaultHeader("Referer", uris.referUri);
            string value = masterCookieList.First(kvp => kvp.Key == "csrftoken").Value; //tag debug
            clientA.AddDefaultHeader("X-CSRFToken", masterCookieList[0].Value);
            clientA.AddDefaultHeader("Accept", "application/json");
            clientA.AddDefaultHeader("Cache-Control", "no-cache");

            var request2 = new RestRequest("/", Method.POST);
            request2.Credentials = new NetworkCredential(login.loginid, login.password);

            request2.AddParameter("application/json", "{}", ParameterType.RequestBody); //TODO: tag test: forces application/json. REMOVE?

            try {
                IRestResponse response2 = clientA.Execute(request2);
                CookiesToCookieList(masterCookieList, response2);
            }
            catch (Exception exception) {
                CombinedLog("\r\nbtnLogin_Click(): Step 2: Server response failure\r\n");
                CombinedLog("\r\n" + exception);
                return;
            }
            CombinedLog("...login complete" + "\r\n");
            loginSuccess = true;

        } //btnLogin_Click

        //----------------------------------------------------------------------------------------------------------
        //tag Early test of ability to retrieve a ToxicologyCase, log it as JSON, and deserialize it.
        //TODO: remove code
        //----------------------------------------------------------------------------------------------------------
        private void btnGetToxCaseByID_Click(object sender, EventArgs e)
        {
            var toxicologyCaseIDStr = tbToxCaseID.Text;

            int toxicologyCaseID;
            try {
                toxicologyCaseID = Convert.ToInt32(toxicologyCaseIDStr);
            }
            catch(Exception exception) {
                CombinedLog("\r\nInvalid: ToxCase ID must be > 0 and not null\r\n");
                CombinedLog(exception.ToString());
                return;
            }
            if (toxicologyCaseID < 1){
                CombinedLog("\r\nInvalid: ToxCase ID must be > 0 and not null\r\n");
                return;
            }

            CombinedLog("\r\n\r\nRetrieving ToxicologyCase #"+toxicologyCaseID +"\r\n");

            var getUriToxicologycase = "api/v1/toxicologycase/" + toxicologyCaseID + "/";
            clientA.BaseUrl = new Uri(uris.referUri + getUriToxicologycase);
            clientA.AddDefaultHeader("Referer", uris.referUri);

            var request = new RestRequest("/", Method.GET);

            request.Credentials = new NetworkCredential(login.loginid, login.password); 

            IRestResponse response = clientA.Execute(request);

            if (response.StatusDescription == "Not Found") {
                CombinedLog("\r\nInvalid Case Number\r\n");
                return;
            }
            string toxString = LogObject(response.Content);
            CombinedLog("ToxicologyCase #"+toxicologyCaseID +" logged to file\r\n");

            CombinedLog("Results: \r\n" + toxString);

            ToxicologyPostFull toxPostFull = JsonConvert.DeserializeObject<ToxicologyPostFull>(response.Content);
            CombinedLog("\r\n\r\n...ToxicologyCase deserialized\r\n");

            CookiesToCookieList(masterCookieList, response);
        }

        //----------------------------------------------------------------------------------------------------------
        //TODO 6/1: TBD: Remove code concerning Operator.  See Issue # 14
        //This is test code; it adds a MultiModalInstrumentOperator to the DB.  At least one is required, so that
        //an Operator can be associated with each Analyte Result.
        private void btnPostOperator_Click(object sender, EventArgs e)
        {
            IRestResponse response = null;
            try {
                var client = new RestClient();

                var postMmInstOperator = "api/v1/multimodalinstrumentoperator/";
                client.BaseUrl = new Uri(uris.referUri + postMmInstOperator);
                client.AddDefaultHeader("Referer", uris.referUri);
                client.AddDefaultHeader("X-CSRFToken", masterCookieList[2].Value);
                client.AddDefaultHeader("Accept", "application/json");
                client.AddDefaultHeader("Cache-Control", "no-cache");
                client.CookieContainer = masterCookieContainer;

                var request = new RestRequest("/", Method.POST);
                request.Credentials = new NetworkCredential(login.loginid, login.password);

                MultiModalInstrumentOperatorPostNS.Object instOp = new MultiModalInstrumentOperatorPostNS.Object();
                instOp.dept = "";
                instOp.first_name = "Good";
                instOp.last_name = "Clinician";
                instOp.middle_name = "";
                instOp.prefix = "Dr.";
                instOp.suffix = "PhD";
                instOp.title = "";
                //instOp.id = null; can't use with POST.

                request.AddJsonBody(instOp);

                response = client.Execute(request);
            }
            catch (Exception exception) {
                CombinedLog("\r\nbtnPostOperator: Exception thrown: " + exception.ToString()+ "\r\n");
                return;
            }
            if (response.StatusDescription == "Created") {
                CombinedLog("\r\nbtnPostOperator \r\n");
                return;
            }
            if (response.StatusDescription == "Not Found") {
                CombinedLog("\r\nbtnPostOperator didn't find endpoint\r\n");
                return;
            }

            CombinedLog("\r\n\r\n...New MultiModalInstrumentOperatorPostNS object created.\r\n");

            CookiesToCookieList(masterCookieList, response);
        }
        //----------------------------------------------------------------------------------------------------------
        private void CookiesToCookieList(List<KeyValuePair<string, string>> cookieList, IRestResponse response) {
            foreach (RestResponseCookie cookie in response.Cookies) {
                cookieList.Add(new KeyValuePair<string, string>(cookie.Name, cookie.Value));
            }
        }
        //----------------------------------------------------------------------------------------------------------
        //invoked on a separate thread; check connectivity status every minute
        private void CheckConnectivity()
        {
            int count = 0;
            try
            {
                RestClient client = new RestClient();
                uris.accountUri = @"https://genalyte-beta.pathagility.com/account/login/";

                client.BaseUrl = new Uri(uris.accountUri);

                while (true)
                {
                    if (form1Closed) {
                        connectThread.Abort();
                    }
                    count++;
                    var request = new RestRequest("/", Method.GET);
                    IRestResponse response = client.Execute(request);

                    if (response.StatusDescription == "OK")
                    {
                        pathagilitySiteUp = true;
                        tbDebug.Invoke((MethodInvoker) delegate {
                            tbConnectStatus.Text = "\r\nConnection UP "; // Running on the UI thread
                            tbConnectStatus.BackColor = Color.GreenYellow;
                        });
                    }
                    else {
                        pathagilitySiteUp = false;
                        tbDebug.Invoke((MethodInvoker) delegate {
                            tbConnectStatus.Text = "\r\nConnection DOWN"; // Running on the UI thread
                            tbConnectStatus.BackColor = Color.Tomato;
                        });
                    }
                    Thread.Sleep(10 * 1000);
                } //while

            } //try
            catch (ThreadAbortException e)
            {
                if (!form1Closed){
                    tbDebug.Invoke((MethodInvoker) delegate {
                        tbConnectStatus.Text =
                            "\r\nCatch: CheckConnectivity() "; // Running on the UI thread
                    });
                }
            }
            finally {
                if (!form1Closed){
                    tbDebug.Invoke((MethodInvoker) delegate {
                        tbConnectStatus.Text =
                            "\r\nFinally: CheckConnectivity() "; // Running on the UI thread
                    });
                }
            }
        }//method

        //----------------------------------------------------------------------------------------------------------
        //Kill the connection thread before exiting the application
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            form1Closed = true;
            try {
                connectThread.Abort();
            }
            catch (Exception exception) {
                CombinedLog("\r\n'Form1_FormClosing(): Ping' thread abort failure");
            }
        }


        //----------------------------------------------------------------------------------------------------------
        //Process barcodes
        //This could be combined with a click event on the scan button, but I want to make sure user clears box.
        //----------------------------------------------------------------------------------------------------------
        private void btnNextPatient_Click(object sender, EventArgs e)
        {
            tbSpecimenIDScan.Text = "";
            specimenIDList.Clear();
            rootSpecimenDataList.Clear();

            try {
                //the List could be empty if we haven't retrieved results...
                rootMmBQList.objects.Clear();
            }
            catch (Exception error) {
                CombinedLog("\r\nDEBUG: Error trying to clear empty multimodalbloodquant list.\r\n");
                CombinedLog(error.ToString());
            }
        }
        //----------------------------------------------------------------------------------------------------------
        //Set focus to "Scan SpecimenIDs" textbox
        //----------------------------------------------------------------------------------------------------------
        private void btnScanStart_Click(object sender, EventArgs e)
        {
            tbSpecimenIDScan.Focus();
        }
        //----------------------------------------------------------------------------------------------------------
        //Add specimen IDs to a list;
        //tag TODO: a faster way to do this?
        //----------------------------------------------------------------------------------------------------------
        private void tbSpecimenIDScan_TextChanged(object sender, EventArgs e)
        {
            var inputText = tbSpecimenIDScan.Text;
            int length = inputText.Length;
            if (length != 0) {  //this happens if "Next Patient" button clears tbSpecimenIDScan
                char last = inputText[length - 1];
                if (last == '\n')
                {
                    CombinedLog("\r\nreceived:" + inputText);
                    specimenIDList.Add(inputText);
                }
            }
        }
        //----------------------------------------------------------------------------------------------------------
        //TODO: Here, we should have all barcodes; what kind of validation checking should I do? Duplicates?
        //----------------------------------------------------------------------------------------------------------
        private void btnScanComplete_Click(object sender, EventArgs e)
        {
            RetrieveAnalytesBySpecimenIDs(specimenIDList);  //See Form1ProcessAnalytes.cs
        }

    }//Form1 class
}//ns
