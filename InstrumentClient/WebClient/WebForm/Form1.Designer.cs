﻿namespace WebFormTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tbUserName = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.tbDebug = new System.Windows.Forms.TextBox();
            this.lblSpecimenIDScan = new System.Windows.Forms.Label();
            this.tbSpecimenIDScan = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.comboSelectDB = new System.Windows.Forms.ComboBox();
            this.lblSelectDB = new System.Windows.Forms.Label();
            this.btnScanComplete = new System.Windows.Forms.Button();
            this.lblPiccoloData = new System.Windows.Forms.Label();
            this.btnPiccoloPatientData = new System.Windows.Forms.Button();
            this.btnEmeraldPatientData = new System.Windows.Forms.Button();
            this.lblEmeraldData = new System.Windows.Forms.Label();
            this.tbToxCaseID = new System.Windows.Forms.TextBox();
            this.btnGetToxCaseByID = new System.Windows.Forms.Button();
            this.lblToxCaseID = new System.Windows.Forms.Label();
            this.lblConnectStatus = new System.Windows.Forms.Label();
            this.tbConnectStatus = new System.Windows.Forms.TextBox();
            this.btnIsedPatientData = new System.Windows.Forms.Button();
            this.lblIsedData = new System.Windows.Forms.Label();
            this.btnIsedControlData = new System.Windows.Forms.Button();
            this.btnEmeraldControlData = new System.Windows.Forms.Button();
            this.btnPiccoloControlData = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnScanStart = new System.Windows.Forms.Button();
            this.btnNextPatient = new System.Windows.Forms.Button();
            this.btnPostOperator = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbUserName
            // 
            this.tbUserName.Location = new System.Drawing.Point(353, 48);
            this.tbUserName.Name = "tbUserName";
            this.tbUserName.Size = new System.Drawing.Size(121, 20);
            this.tbUserName.TabIndex = 2;
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(353, 86);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '-';
            this.tbPassword.Size = new System.Drawing.Size(121, 20);
            this.tbPassword.TabIndex = 3;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(287, 51);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(60, 13);
            this.lblUserName.TabIndex = 2;
            this.lblUserName.Text = "User Name";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(290, 89);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password";
            // 
            // tbDebug
            // 
            this.tbDebug.Location = new System.Drawing.Point(51, 430);
            this.tbDebug.Multiline = true;
            this.tbDebug.Name = "tbDebug";
            this.tbDebug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbDebug.Size = new System.Drawing.Size(876, 199);
            this.tbDebug.TabIndex = 10;
            // 
            // lblSpecimenIDScan
            // 
            this.lblSpecimenIDScan.AutoSize = true;
            this.lblSpecimenIDScan.Location = new System.Drawing.Point(552, 51);
            this.lblSpecimenIDScan.Name = "lblSpecimenIDScan";
            this.lblSpecimenIDScan.Size = new System.Drawing.Size(87, 13);
            this.lblSpecimenIDScan.TabIndex = 5;
            this.lblSpecimenIDScan.Text = "Scan Specimens";
            // 
            // tbSpecimenIDScan
            // 
            this.tbSpecimenIDScan.Location = new System.Drawing.Point(651, 48);
            this.tbSpecimenIDScan.Multiline = true;
            this.tbSpecimenIDScan.Name = "tbSpecimenIDScan";
            this.tbSpecimenIDScan.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSpecimenIDScan.Size = new System.Drawing.Size(276, 99);
            this.tbSpecimenIDScan.TabIndex = 5;
            this.tbSpecimenIDScan.TextChanged += new System.EventHandler(this.tbSpecimenIDScan_TextChanged);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(398, 124);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // comboSelectDB
            // 
            this.comboSelectDB.FormattingEnabled = true;
            this.comboSelectDB.Items.AddRange(new object[] {
            "genalyte-beta",
            "genalyte-demo"});
            this.comboSelectDB.Location = new System.Drawing.Point(135, 48);
            this.comboSelectDB.Name = "comboSelectDB";
            this.comboSelectDB.Size = new System.Drawing.Size(121, 21);
            this.comboSelectDB.TabIndex = 1;
            this.comboSelectDB.SelectedIndexChanged += new System.EventHandler(this.comboSelectDB_SelectedIndexChanged);
            // 
            // lblSelectDB
            // 
            this.lblSelectDB.AutoSize = true;
            this.lblSelectDB.Location = new System.Drawing.Point(48, 51);
            this.lblSelectDB.Name = "lblSelectDB";
            this.lblSelectDB.Size = new System.Drawing.Size(86, 13);
            this.lblSelectDB.TabIndex = 11;
            this.lblSelectDB.Text = "Select Database";
            // 
            // btnScanComplete
            // 
            this.btnScanComplete.Location = new System.Drawing.Point(840, 168);
            this.btnScanComplete.Name = "btnScanComplete";
            this.btnScanComplete.Size = new System.Drawing.Size(87, 23);
            this.btnScanComplete.TabIndex = 7;
            this.btnScanComplete.Text = "Scan Complete";
            this.btnScanComplete.UseVisualStyleBackColor = true;
            this.btnScanComplete.Click += new System.EventHandler(this.btnScanComplete_Click);
            // 
            // lblPiccoloData
            // 
            this.lblPiccoloData.AutoSize = true;
            this.lblPiccoloData.Location = new System.Drawing.Point(48, 259);
            this.lblPiccoloData.Name = "lblPiccoloData";
            this.lblPiccoloData.Size = new System.Drawing.Size(68, 13);
            this.lblPiccoloData.TabIndex = 13;
            this.lblPiccoloData.Text = "Piccolo Data";
            // 
            // btnPiccoloPatientData
            // 
            this.btnPiccoloPatientData.Location = new System.Drawing.Point(228, 254);
            this.btnPiccoloPatientData.Name = "btnPiccoloPatientData";
            this.btnPiccoloPatientData.Size = new System.Drawing.Size(75, 23);
            this.btnPiccoloPatientData.TabIndex = 8;
            this.btnPiccoloPatientData.Text = "Patient";
            this.btnPiccoloPatientData.UseVisualStyleBackColor = true;
            this.btnPiccoloPatientData.Click += new System.EventHandler(this.btnPiccoloPatientData_Click);
            // 
            // btnEmeraldPatientData
            // 
            this.btnEmeraldPatientData.Location = new System.Drawing.Point(228, 285);
            this.btnEmeraldPatientData.Name = "btnEmeraldPatientData";
            this.btnEmeraldPatientData.Size = new System.Drawing.Size(75, 23);
            this.btnEmeraldPatientData.TabIndex = 9;
            this.btnEmeraldPatientData.Text = "Patient";
            this.btnEmeraldPatientData.UseVisualStyleBackColor = true;
            this.btnEmeraldPatientData.Click += new System.EventHandler(this.btnEmeraldPatientData_Click);
            // 
            // lblEmeraldData
            // 
            this.lblEmeraldData.AutoSize = true;
            this.lblEmeraldData.Location = new System.Drawing.Point(48, 290);
            this.lblEmeraldData.Name = "lblEmeraldData";
            this.lblEmeraldData.Size = new System.Drawing.Size(71, 13);
            this.lblEmeraldData.TabIndex = 15;
            this.lblEmeraldData.Text = "Emerald Data";
            // 
            // tbToxCaseID
            // 
            this.tbToxCaseID.Enabled = false;
            this.tbToxCaseID.Location = new System.Drawing.Point(806, 317);
            this.tbToxCaseID.Name = "tbToxCaseID";
            this.tbToxCaseID.Size = new System.Drawing.Size(121, 20);
            this.tbToxCaseID.TabIndex = 16;
            // 
            // btnGetToxCaseByID
            // 
            this.btnGetToxCaseByID.Enabled = false;
            this.btnGetToxCaseByID.Location = new System.Drawing.Point(806, 355);
            this.btnGetToxCaseByID.Name = "btnGetToxCaseByID";
            this.btnGetToxCaseByID.Size = new System.Drawing.Size(121, 23);
            this.btnGetToxCaseByID.TabIndex = 17;
            this.btnGetToxCaseByID.Text = "Get ToxCase";
            this.btnGetToxCaseByID.UseVisualStyleBackColor = true;
            this.btnGetToxCaseByID.Click += new System.EventHandler(this.btnGetToxCaseByID_Click);
            // 
            // lblToxCaseID
            // 
            this.lblToxCaseID.AutoSize = true;
            this.lblToxCaseID.Enabled = false;
            this.lblToxCaseID.Location = new System.Drawing.Point(700, 320);
            this.lblToxCaseID.Name = "lblToxCaseID";
            this.lblToxCaseID.Size = new System.Drawing.Size(91, 13);
            this.lblToxCaseID.TabIndex = 18;
            this.lblToxCaseID.Text = "Enter ToxCase ID";
            // 
            // lblConnectStatus
            // 
            this.lblConnectStatus.AutoSize = true;
            this.lblConnectStatus.Location = new System.Drawing.Point(48, 89);
            this.lblConnectStatus.Name = "lblConnectStatus";
            this.lblConnectStatus.Size = new System.Drawing.Size(80, 13);
            this.lblConnectStatus.TabIndex = 19;
            this.lblConnectStatus.Text = "Connect Status";
            // 
            // tbConnectStatus
            // 
            this.tbConnectStatus.Location = new System.Drawing.Point(135, 86);
            this.tbConnectStatus.Name = "tbConnectStatus";
            this.tbConnectStatus.Size = new System.Drawing.Size(121, 20);
            this.tbConnectStatus.TabIndex = 20;
            // 
            // btnIsedPatientData
            // 
            this.btnIsedPatientData.Location = new System.Drawing.Point(228, 314);
            this.btnIsedPatientData.Name = "btnIsedPatientData";
            this.btnIsedPatientData.Size = new System.Drawing.Size(75, 23);
            this.btnIsedPatientData.TabIndex = 21;
            this.btnIsedPatientData.Text = "Patient";
            this.btnIsedPatientData.UseVisualStyleBackColor = true;
            this.btnIsedPatientData.Click += new System.EventHandler(this.btnIsedPatientData_Click);
            // 
            // lblIsedData
            // 
            this.lblIsedData.AutoSize = true;
            this.lblIsedData.Location = new System.Drawing.Point(48, 319);
            this.lblIsedData.Name = "lblIsedData";
            this.lblIsedData.Size = new System.Drawing.Size(57, 13);
            this.lblIsedData.TabIndex = 22;
            this.lblIsedData.Text = "iSED Data";
            // 
            // btnIsedControlData
            // 
            this.btnIsedControlData.Location = new System.Drawing.Point(135, 314);
            this.btnIsedControlData.Name = "btnIsedControlData";
            this.btnIsedControlData.Size = new System.Drawing.Size(75, 23);
            this.btnIsedControlData.TabIndex = 25;
            this.btnIsedControlData.Text = "Control";
            this.btnIsedControlData.UseVisualStyleBackColor = true;
            this.btnIsedControlData.Click += new System.EventHandler(this.btnIsedControlData_Click);
            // 
            // btnEmeraldControlData
            // 
            this.btnEmeraldControlData.Location = new System.Drawing.Point(135, 285);
            this.btnEmeraldControlData.Name = "btnEmeraldControlData";
            this.btnEmeraldControlData.Size = new System.Drawing.Size(75, 23);
            this.btnEmeraldControlData.TabIndex = 24;
            this.btnEmeraldControlData.Text = "Control";
            this.btnEmeraldControlData.UseVisualStyleBackColor = true;
            this.btnEmeraldControlData.Click += new System.EventHandler(this.btnEmeraldControlData_Click);
            // 
            // btnPiccoloControlData
            // 
            this.btnPiccoloControlData.Location = new System.Drawing.Point(135, 254);
            this.btnPiccoloControlData.Name = "btnPiccoloControlData";
            this.btnPiccoloControlData.Size = new System.Drawing.Size(75, 23);
            this.btnPiccoloControlData.TabIndex = 23;
            this.btnPiccoloControlData.Text = "Control";
            this.btnPiccoloControlData.UseVisualStyleBackColor = true;
            this.btnPiccoloControlData.Click += new System.EventHandler(this.btnPiccoloControlData_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 224);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Generate Simulated Data";
            // 
            // btnScanStart
            // 
            this.btnScanStart.Location = new System.Drawing.Point(747, 168);
            this.btnScanStart.Name = "btnScanStart";
            this.btnScanStart.Size = new System.Drawing.Size(87, 23);
            this.btnScanStart.TabIndex = 27;
            this.btnScanStart.Text = "Start Scan";
            this.btnScanStart.UseVisualStyleBackColor = true;
            this.btnScanStart.Click += new System.EventHandler(this.btnScanStart_Click);
            // 
            // btnNextPatient
            // 
            this.btnNextPatient.Location = new System.Drawing.Point(651, 168);
            this.btnNextPatient.Name = "btnNextPatient";
            this.btnNextPatient.Size = new System.Drawing.Size(87, 23);
            this.btnNextPatient.TabIndex = 28;
            this.btnNextPatient.Text = "Next Patient";
            this.btnNextPatient.UseVisualStyleBackColor = true;
            this.btnNextPatient.Click += new System.EventHandler(this.btnNextPatient_Click);
            // 
            // btnPostOperator
            // 
            this.btnPostOperator.Enabled = false;
            this.btnPostOperator.Location = new System.Drawing.Point(135, 124);
            this.btnPostOperator.Name = "btnPostOperator";
            this.btnPostOperator.Size = new System.Drawing.Size(121, 23);
            this.btnPostOperator.TabIndex = 29;
            this.btnPostOperator.Text = "POST Operator";
            this.btnPostOperator.UseVisualStyleBackColor = true;
            this.btnPostOperator.Click += new System.EventHandler(this.btnPostOperator_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.btnPostOperator);
            this.Controls.Add(this.btnNextPatient);
            this.Controls.Add(this.btnScanStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnIsedControlData);
            this.Controls.Add(this.btnEmeraldControlData);
            this.Controls.Add(this.btnPiccoloControlData);
            this.Controls.Add(this.btnIsedPatientData);
            this.Controls.Add(this.lblIsedData);
            this.Controls.Add(this.tbConnectStatus);
            this.Controls.Add(this.lblConnectStatus);
            this.Controls.Add(this.lblToxCaseID);
            this.Controls.Add(this.btnGetToxCaseByID);
            this.Controls.Add(this.tbToxCaseID);
            this.Controls.Add(this.btnEmeraldPatientData);
            this.Controls.Add(this.lblEmeraldData);
            this.Controls.Add(this.btnPiccoloPatientData);
            this.Controls.Add(this.lblPiccoloData);
            this.Controls.Add(this.btnScanComplete);
            this.Controls.Add(this.lblSelectDB);
            this.Controls.Add(this.comboSelectDB);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.tbSpecimenIDScan);
            this.Controls.Add(this.lblSpecimenIDScan);
            this.Controls.Add(this.tbDebug);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbUserName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Genalyte Web API Test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbUserName;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox tbDebug;
        private System.Windows.Forms.Label lblSpecimenIDScan;
        private System.Windows.Forms.TextBox tbSpecimenIDScan;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.ComboBox comboSelectDB;
        private System.Windows.Forms.Label lblSelectDB;
        private System.Windows.Forms.Button btnScanComplete;
        private System.Windows.Forms.Label lblPiccoloData;
        private System.Windows.Forms.Button btnPiccoloPatientData;
        private System.Windows.Forms.Button btnEmeraldPatientData;
        private System.Windows.Forms.Label lblEmeraldData;
        private System.Windows.Forms.TextBox tbToxCaseID;
        private System.Windows.Forms.Button btnGetToxCaseByID;
        private System.Windows.Forms.Label lblToxCaseID;
        private System.Windows.Forms.Label lblConnectStatus;
        private System.Windows.Forms.TextBox tbConnectStatus;
        private System.Windows.Forms.Button btnIsedPatientData;
        private System.Windows.Forms.Label lblIsedData;
        private System.Windows.Forms.Button btnIsedControlData;
        private System.Windows.Forms.Button btnEmeraldControlData;
        private System.Windows.Forms.Button btnPiccoloControlData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnScanStart;
        private System.Windows.Forms.Button btnNextPatient;
        private System.Windows.Forms.Button btnPostOperator;
    }
}

