﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenalyteMobile.DeviceData
{
    public class EmeraldPatientData
    {
        public String sampleType { get; set; }
        public String patientID { get; set; }
        public String diskLotNumber { get; set; }
        public String serialNumber { get; set; }
        public String WBC { get; set; }
        public String RBC { get; set; }
        public String HGB { get; set; }
        public String HCT { get; set; }
        public String MCV { get; set; }
        public String MCH { get; set; }
        public String MCHC { get; set; }
        public String RDW { get; set; }
        public String PLT { get; set; }
        public String MPV { get; set; }
        public String LYMPercent { get; set; }
        public String MIDPercent { get; set; }
        public String GRAPercent { get; set; }
        public String LYM { get; set; }
        public String MID { get; set; }
        public String GRA { get; set; }

        //ctor
        public EmeraldPatientData()
        {
            sampleType = "sampleType";
            patientID = "patientID";
            diskLotNumber = "diskLotNumber";
            serialNumber = "serialNumber";
            WBC = "WBC";
            RBC = "RBC";
            HGB = "HGB";
            HCT = "HCT";
            MCV = "MCV";
            MCH = "MCH";
            MCHC = "MCHC";
            RDW = "RDW";
            PLT = "PLT";
            MPV = "MPV";
            LYMPercent = "LYMPercent";
            MIDPercent = "MIDPercent";
            GRAPercent = "GRAPercent";
            LYM = "LYM";
            MID = "MID";
            GRA = "GRA";
            
        }

    }
}
