﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenalyteMobile.DeviceData
{
    public class iSEDPatientData
    {
        public String instrumentNumber { get; set; }    // 00 to 99
        public String patientID { get; set; }           // max 30 char
        public String sampleID { get; set; }            // max 30 char
        public String measurementValue { get; set; }    // from 0 to 130 unit always mm/h
        public String abnormalFlag { get; set; }        // < or >
        public String resultStatus { get; set; }        // P or X
        public String instrumentID { get; set; }        // 01 to 99

        public iSEDPatientData()
        {
            instrumentNumber  =   "instrumentNumber";  
            patientID  =   "patientID";          
            sampleID  =   "sampleID";         
            measurementValue  =   "measurementValue";  
            abnormalFlag  =   "abnormalFlag";      
            resultStatus  =   "resultStatus";      
            instrumentID  =   "instrumentID";      
        }
    }   


}
