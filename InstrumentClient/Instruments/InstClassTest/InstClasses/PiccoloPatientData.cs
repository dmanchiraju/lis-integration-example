﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenalyteMobile.DeviceData
{
    public class PiccoloPatientData
    {
        public String sampleType { get; set; }
        public String patientID { get; set; }
        public String specimenID { get; set; }
        public String diskLotNumber { get; set; }
        public String serialNumber { get; set; }

        // test parameters results including the units
        public String NA { get; set; }
        public String K { get; set; }
        public String tCO2 { get; set; }
        public String CL { get; set; }
        public String AG { get; set; }
        public String GLU { get; set; }
        public String CA { get; set; }
        public String BUN { get; set; }
        public String CRE { get; set; }
        public String ALP { get; set; }
        public String ALT { get; set; }
        public String AST { get; set; }
        public String TBIL { get; set; }
        public String ALB { get; set; }
        public String TP { get; set; }

        //ctor
        public PiccoloPatientData()
        {
            sampleType = "sampletype";
            patientID = "patientID";;
            specimenID = "specimenID";
            diskLotNumber = "diskLotNumber";
            serialNumber = "instrumentSerialNumber";
            NA = "NA";
            K = "K";
            tCO2 = "tCO2";
            CL  = "CL";
            AG  = "AG";
            GLU = "GLU"; 
            CA  = "CA";
            BUN  = "BUN";
            CRE  = "CRE";
            ALP  = "ALP";
            ALT  = "ALT";
            AST  = "AST";
            TBIL = "TBIL";
            ALB = "ALB";
            TP  = "TP";
        }
    }
}
